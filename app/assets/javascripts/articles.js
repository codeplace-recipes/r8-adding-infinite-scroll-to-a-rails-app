$(document).on("turbolinks:load", function() {
  if ($(".pagination").length) {
    $(window).scroll(function() {
      let url = $(".pagination > .next > a").attr("href");
      if (url && ($(window).scrollTop() > ($(document).height() - $(window).height() - 50))) {
        $(".pagination").text("Loading articles...");
        $.getScript(url);
      }
    });
    $(window).scroll();
  }
});
